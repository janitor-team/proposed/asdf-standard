1.0.2 (2022-04-15)
------------------

- Pin astropy min version to 5.0.4. [#310]

1.0.1 (2022-02-23)
------------------

- Remove asdf as an install dependency for the asdf-standard package. [#300]

1.0.0 (2022-02-14)
-------------------

- Add installable Python package to replace use of this repo as a submodule.  [#292]
